﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Aplicacao.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Aplicacao.Controllers
{
    public class ArquivosController : Controller
    {
        ArquivoDbContext _context;
        public ArquivosController(ArquivoDbContext contexto)
        {
            _context = contexto;
        }
        [HttpGet]
        public IActionResult Index()
        {
            List<int> arquivos = _context.Arquivo.Select(m => m.Id).ToList();
            return View(arquivos);
        }


        [HttpPost]
        public IActionResult UploadArquivo(IList<IFormFile> arquivos)
        {
            IFormFile arquivoEnviado = arquivos.FirstOrDefault();
            if (arquivoEnviado != null || arquivoEnviado.ContentType.ToLower().StartsWith("arquivo/"))
            {
                MemoryStream ms = new MemoryStream();
                arquivoEnviado.OpenReadStream().CopyTo(ms);
                Arquivo arquivoEntity = new Arquivo()
                {
                    Nome = arquivoEnviado.Name,
                    Dados = ms.ToArray(),
                    ContentType = arquivoEnviado.ContentType
                };
                _context.Arquivo.Add(arquivoEntity);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        [HttpGet]
        public FileStreamResult VerArquivo(int id)
        {
            Arquivo arquivo = _context.Arquivo.FirstOrDefault(m => m.Id == id);
            MemoryStream ms = new MemoryStream(arquivo.Dados);
            return new FileStreamResult(ms, arquivo.ContentType);
        }
    }
}