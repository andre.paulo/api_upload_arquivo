﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aplicacao.Models
{
    public class ArquivoDbContext: DbContext
    {
        public ArquivoDbContext(DbContextOptions<ArquivoDbContext> options)
            : base(options)
        { }
        public DbSet<Arquivo> Arquivo { get; set; }
    }
}
